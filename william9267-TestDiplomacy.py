#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Madrid Move London"
        a = diplomacy_read(s)
        self.assertEqual(a[0],  "A")
        self.assertEqual(a[1], "Madrid")
        self.assertEqual(a[2], "Move")
        self.assertEqual(a[3], "London")
	
    def test_read_2(self):
        s = "B London Hold"
        a = diplomacy_read(s)
        self.assertEqual(a[0],  "B")
        self.assertEqual(a[1], "London")
        self.assertEqual(a[2], "Hold")

    def test_read_3(self):
        s = "C Beijing Support"
        a = diplomacy_read(s)
        self.assertEqual(a[0], "C")
        self.assertEqual(a[1], "Beijing")
        self.assertEqual(a[2], "Support")

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, 'A', '[dead]')
        self.assertEqual(w.getvalue(), "A [dead]\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, 'B', 'London')
        self.assertEqual(w.getvalue(), "B London\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, 'C', '[dead]')
        self.assertEqual(w.getvalue(), "C [dead]\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB London Hold\nC Houston Hold\nD Beijing Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB London\nC Houston\nD Beijing\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Move London\nB London Hold\nC Houston Move London\nD Beijing Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Beijing\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Support B\nB London Hold\nC Houston Move London\nD Beijing Support C\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB [dead]\nC [dead]\nD Beijing\n")

    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB London Hold\nC Houston Move London\nD Beijing Support C\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB [dead]\nC London\nD Beijing\n")

    def test_solve_5(self):
        r = StringIO("A Madrid Hold\nB London Hold\nC Houston Move London\nD Beijing Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB London\nC [dead]\nD Beijing\n")
            
    def test_solve_6(self):
        r = StringIO("A Madrid Move Beijing\nB London Hold\nC Houston Move London\nD Beijing Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

# ----
# main
# ----


if __name__ == "__main__": #pragma: no cover
    main()
