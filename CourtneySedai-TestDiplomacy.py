#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestCollatz
# -----------


class TestDiplomacy (TestCase):
    
    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Rome Hold\nB Tipperary Move Rome\nC Milan Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Rome\nC Milan\n")

    def test_solve_2(self):
        r = StringIO("A Rome Hold\nB Florence Move Rome\nC Milan Move Rome")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")
    
    def test_solve_3(self):
        r = StringIO("A Rome Hold\nB Tipperary Move Rome\nC Milan Support A\nD Florence Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC Milan\nD Florence\n")
    
    def test_solve_4(self):
        r = StringIO("A Rome Hold\nB Tipperary Move Rome\nC Milan Move Rome\nL HongKong Move Rome\nD Florence Support B\nE Vatican Support C\nF Sicily Support C\nG London Support L\nH Edinburgh Support L\nI Cardiff Support L")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nL Rome\nD Florence\nE Vatican\nF Sicily\nG London\nH Edinburgh\nI Cardiff\n")
    
    def test_solve_5(self):
        r = StringIO("A Rome Hold\nB Tipperary Move Rome\nC Milan Move Rome\nL HongKong Move Rome\nD Florence Support B\nE Vatican Support C\nF Sicily Support C\nG London Support A\nH Edinburgh Support A\nI Cardiff Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Rome\nB [dead]\nC [dead]\nL [dead]\nD Florence\nE Vatican\nF Sicily\nG London\nH Edinburgh\nI Cardiff\n")
    
    def test_solve_6(self):
        r = StringIO("A Cardiff Move London\nB Austin Move Houston\nC Houston Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A London\nB [dead]\nC [dead]\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()