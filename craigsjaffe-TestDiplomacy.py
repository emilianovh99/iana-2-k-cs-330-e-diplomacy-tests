#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read,diplomacy_solve


# -----------
# Testdiplomacy
# -----------


class Testdiplomacy (TestCase):
   
    # -----
    # solve
    # -----
    
    #Test Solve 1
    def test_solve(self):
        r = StringIO("A Madrid Hold \nB Barcelona Support A \nC London Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Barcelona\nC [DEAD]\n")
          
    #Test Solve 2        
    def test_solve2(self):
        r = StringIO("A Austin Hold \nB Houston Move Austin \nC Plano Support B \nD Paris Move Plano")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [DEAD]\nB [DEAD]\nC [DEAD]\nD [DEAD]\n")
            
    #Test Solve 3        
    def test_solve3(self):
        r = StringIO("A Madrid Hold \nB Barcelona Move Madrid \nC London Move Madrid \nD Paris Support B \nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [DEAD]\nB [DEAD]\nC [DEAD]\nD Paris\nE Austin\n")
    
    #Test Solve 4      
    def test_solve4(self):
        r = StringIO("A Madrid Hold \nB Barcelona Move Madrid \nC London Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [DEAD]\nB [DEAD]\nC [DEAD]\n")
    
    #Test Solve 5      
    def test_solve5(self):
        r = StringIO("A Madrid Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")
    
# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch Testdiplomacy.py >  Testdiplomacy.out 2>&1


$ cat Testdiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> Testdiplomacy.out



$ cat Testdiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
diplomacy.py          12      0      2      0   100%
Testdiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
