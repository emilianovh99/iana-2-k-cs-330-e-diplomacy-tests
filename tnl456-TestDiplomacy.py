#!/usr/bin/env python3

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestCollatz
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    # Initial Test
    def test_read_1(self):
        s = "A Madrid Hold"
        i = diplomacy_read(s)
        self.assertEqual(i, [['A', 'Madrid', 'Hold']])

    def test_read_2(self):
        s = "B NewYork Support A"
        i = diplomacy_read(s)
        self.assertEqual(i, [['B', 'NewYork', 'Support', 'A']])

    def test_read_3(self):
        s = "Z Awidkwocwo Move Boston"
        i = diplomacy_read(s)
        self.assertEqual(i, [['Z', 'Awidkwocwo', 'Move', 'Boston']])

    def test_read_4(self):
        s = "X AwDFWdCewSdfE Hold"
        i = diplomacy_read(s)
        self.assertEqual(i, [['X', 'AwDFWdCewSdfE', 'Hold']])
        
    def test_read_5(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid"
        i = diplomacy_read(s)
        self.assertEqual(i, [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Move', 'Madrid']])

    # ----
    # eval
    # ----

    # Initial tests
    """
    def test_eval_1(self):
        v = diplomacy_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = diplomacy_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = diplomacy_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = diplomacy_eval(900, 1000)
        self.assertEqual(v, 174)
    """
    
    # -----
    # print
    # -----

    # Initial test
    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, {'A': ['Madrid']})
        self.assertEqual(w.getvalue(), "A Madrid\n")

    # Tests that it still works when values given are the same
    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, {'B': ['Barcelona']})
        self.assertEqual(w.getvalue(), "B Barcelona\n")

    # Tests that it still works when reversing the values
    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, {'Z': ['AfbjWckdWccASdED']})
        self.assertEqual(w.getvalue(), "Z AfbjWckdWccASdED\n")

    # -----
    # solve
    # -----

    # Initial Test
    def test_solve_1(self):
        r = StringIO("A Madrid Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")

    # Tests over a large range
    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    # Tests at the very edge of the range
    def test_solve_3(self):
        r = StringIO("A Austin Move Houston\nB Houston Move Austin")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Houston\nB Austin\n")
            
    def test_solve_4(self):
        r = StringIO("A Austin Support C\nB Barcelona Support F\nC Chicago Move Madrid\nD Dunkirk Move Barcelona\nE Edmonton Support B\nF Fullerton Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Austin\nB Barcelona\nC Madrid\nD [dead]\nE Edmonton\nF [dead]\n")
            
    def test_solve_5(self):
        r = StringIO("A Austin Hold\nB Barcelona Move Austin\nC Chicago Support B\nD Dunkirk Support B\nE Edmonton Move Austin\nF Fullerton Move Austin")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Austin\nC Chicago\nD Dunkirk\nE [dead]\nF [dead]\n")
# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.............
----------------------------------------------------------------------
Ran 13 tests in 0.002s

OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.............
----------------------------------------------------------------------
Ran 13 tests in 0.002s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          72      0     46      1    99%   66->63
TestDiplomacy.py      63      0      2      0   100%
--------------------------------------------------------------
TOTAL                135      0     48      1    99%

"""
