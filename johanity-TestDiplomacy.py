#!/usr/bin/env python3

# Johan David Bonilla
# Hope this file finds you well!

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_solve
from Diplomacy import Warzone
from Diplomacy import Diplomacy

# -----------
# TestDiplomacy
# -----------

class TestDiplomacy (TestCase):
 
    # ----
    # read
    # ----

    def readingTest1(self):
        war = Warzone() #creates a warzone
        move = "A Madrid Hold" #creates a move
        result = war.read(move) #reads the move
        self.assertEqual(result, ("A", "Madrid", "Hold")) #checks if the move is correct

    def readingTest2(self):
        war = Warzone() #creates a warzone
        move = "C London Support B" #creates a move
        result = war.read(move) #reads the move
        self.assertEqual(result, ("C", "London", "Support", "B")) #check if the move is correct
 
    def readingTest3(self):
        war = Warzone() #creates a warzone
        move = "B Barcelona Move Madrid" #creates a move
        result = war.read(move) #reads the move
        self.assertEqual(result, ("B", "Barcelona", "Move", "Madrid")) #check if the move is correct
        
    # ----
    # eval
    # ----

    def evaluationTest(self):
        war = Warzone() #creates a warzone
        war.update(('A', 'Madrid', 'Hold')) #adds an army to the warzone
        war.update(('B', 'Barcelona', 'Move', 'Madrid')) #adds an army to the warzone
        war.update(('C', 'London', 'Support', 'B')) #
        result = war.status_check() #checks the status of the warzone
        self.assertEqual(result, [('A', '[dead]'), ('B', 'Madrid'), ('C', 'London')]) #checks if the status is correct

    # -----
    # print
    # -----

    def printTest(self):
        war = Warzone() #creates a warzone
        war.update(('A', 'Madrid', 'Hold'))  #adds an army to the warzone
        war.update(('B', 'Barcelona', 'Move', 'Madrid')) #adds an army to the warzone
        war.update(('C', 'London', 'Support', 'B')) #adds an army to the warzone
        w = StringIO() #creates a string
        war.print(w) #prints the warzone
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n") # checks assertions


    #testing diplomacy.update() function

    def updater(self): 
        procedure = Warzone() #creates a warzone
        procedure = Warzone().update(('A', 'London', 'Move', 'Madrid')) #adds an army to the warzone
        result = procedure.status_check() #checks the status of the warzone
        self.assertEqual(result, [('A', 'Madrid')]) #checking if the function is working correctly

    # -----
    # solve
    # -----

    def diplomacyTest1(self):

        r = StringIO("A Madrid Hold") #reads the input
        w = StringIO() #writer

        diplomacy_solve(r, w)

        self.assertEqual( #checks if the output is correct
            w.getvalue(), "A Madrid\n")

    def diplomacyTest2(self):
        
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid") #reads the input
        w = StringIO() #writer

        diplomacy_solve(r, w) #calls the function

        self.assertEqual( #checks if the output is correct
            w.getvalue(), "A [dead]\nB [dead]\n")

    def diplomacyTest3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n") #
        w = StringIO() #writer
        diplomacy_solve(r, w) #calls the function
        self.assertEqual( #checks if the output is correct
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")
    
# ----
# main
# ----

if __name__ == "__main__":
    main()
