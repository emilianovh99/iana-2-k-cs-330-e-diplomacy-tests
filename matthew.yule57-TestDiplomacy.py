from unittest import main, TestCase
from Diplomacy import diplomacy_solve

class TestDiplomacy(TestCase):
	def test_1(self):
		i = '''A Madrid Move Barcelona
B Barcelona Move Madrid
C London Move Austin
D Austin Move London'''

		o = '''A Barcelona
B Madrid
C Austin
D London'''

		r = diplomacy_solve(i)
		self.assertEqual(r, o)

	def test_2(self):
		i = '''A Madrid Hold 
B Barcelona Move Madrid
C London Support B'''

		o = '''A [dead]
B Madrid
C London'''

		r = diplomacy_solve(i)
		self.assertEqual(r, o)
	
	def test_3(self):
		i = '''A Madrid Hold
B Barcelona Move Madrid
C London Move Madrid
D Paris Support B
E Austin Support A'''

		o = '''A [dead]
B [dead]
C [dead]
D Paris
E Austin'''

		r = diplomacy_solve(i)
		self.assertEqual(r, o)

	def test_4(self):
		i = '''A Madrid Support B
B Barcelona Hold
C Getafe Move Madrid
D Lisbon Move Barcelona'''

		o = '''A [dead]
B [dead]
C [dead]
D [dead]'''

		r = diplomacy_solve(i)
		self.assertEqual(r, o)
	
if __name__ == "__main__": # pragma: no cover
    main()