# !/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from Diplomacy import diplomacy_read, diplomacy_print, diplomacy_solve
from unittest import main, TestCase

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Austin Hold\n"
        a = diplomacy_read(s)
        self.assertEqual(a,  ['A', 'Austin', 'Hold'])

    def test_read_2(self):
        s = "B Taipei Move Taichung\n"
        a = diplomacy_read(s)
        self.assertEqual(a,  ['B', 'Taipei', 'Move', 'Taichung'])


    def test_read_3(self):
        s = "C Tokyo Support Osaka\n"
        a = diplomacy_read(s)
        self.assertEqual(a,  ['C', 'Tokyo', 'Support', 'Osaka'])    # ----


    # evalTep
    # ----

    
    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, 'A', '[dead]')
        self.assertEqual(w.getvalue(), "A [dead]\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, 'D', 'Dallas')
        self.assertEqual(w.getvalue(), "D Dallas\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, 'B', '[dead]')
        self.assertEqual(w.getvalue(), "B [dead]\n")

    # -----
    # solve
    # -----
    
    def test_solve_1(self):
        r = StringIO("A Austin Hold\nB Tokyo Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Austin\nB Tokyo\n")
    
    
    def test_solve_2(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")

   
    def test_solve_3(self):
        r = StringIO("B Taipei Hold\nA Kaoshiung Move Houston\nC Houston Move Kaoshiung\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Houston\nB Taipei\nC Kaoshiung\n")

    
    def test_solve_4(self):
        r = StringIO("A Madrid Support B\nB Lahore Move Seattle\nC Seattle Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Seattle\nC [dead]\n")

    def test_solve_5(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Move Madrid\nC London Move Austin\nD Austin Move London")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Barcelona\nB Madrid\nC Austin\nD London\n")

    def test_solve_6(self):
        r = StringIO("A Madrid Support B\nB Austin Hold\nC Houston Move Austin")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Austin\nC [dead]\n")

    def test_solve_7(self):
        r = StringIO("A Austin Support B\nB Boston Support C\nC Charlotte Hold\nD Dallas Move Boston\nE Edmonton Move Charlotte\nF FortWorth Support E")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Austin\nB Boston\nC [dead]\nD [dead]\nE Charlotte\nF FortWorth\n")

    def test_solve_8(self):
        r = StringIO("A Austin Move Tokyo\nB Boston Support C\nC Charlotte Hold\nD Dallas Move Boston\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Tokyo\nB [dead]\nC Charlotte\nD [dead]\n")

    

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
