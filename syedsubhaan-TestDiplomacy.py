
from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, \
                      diplomacy_solve, army_class, city_class


test_inputs = ['A Madrid Hold\n',
               'A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n',
               'A Madrid Hold\nB Barcelona Move Madrid\n',
               'A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n',
               'A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n',
               'A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n',
               'A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n']

test_outputs = ['A Madrid\n',
                'A [dead]\nB Madrid\nC London\n',
                'A [dead]\nB [dead]\n',
               'A [dead]\nB [dead]\nC [dead]\nD [dead]\n',
                'A [dead]\nB [dead]\nC [dead]\n',
                'A [dead]\nB Madrid\nC [dead]\nD Paris\n',
                'A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n']


class TestDiplomacy (TestCase):

    # Missing an action
    def test_args_1(self):
        r = StringIO('A Madrid\n')
        with self.assertRaises(Exception):
            armytest, citytest = diplomacy_read(r)

    # Invalid Action
    def test_args_2(self):
        r = StringIO('A Madrid Drive\n')
        with self.assertRaises(Exception):
            armytest, citytest = diplomacy_read(r)

    # Support/Move missing info
    def test_args_3(self):
        r = StringIO('A Madrid Move\n')
        with self.assertRaises(Exception):
            armytest, citytest = diplomacy_read(r)

    #
    def test_args_4(self):
        r = StringIO('A Madrid Hold Madrid\n')
        armytest, citytest = diplomacy_read(r)
        assert armytest['A'].destination == None

    def test_args_5(self):
        r = StringIO('A Madrid Hold Madrid Deez Nuts I Love this CLass\n')
        with self.assertRaises(Exception):
            armytest, citytest = diplomacy_read(r)




    # READ
    def test_read_1(self):
        r = StringIO('A Madrid Hold\n')
        armytest, citytest = diplomacy_read(r)

        armies = {}
        armies['A'] = army_class('A', 'Madrid', 'Hold')

        cities = {}
        cities['Madrid'] = city_class('Madrid', 'A')

        for army in armies:
            self.assertEqual(armies[army], armytest[army])

        for city in cities:
            assert cities[city] == citytest[city]

    def test_read_2(self):
        s = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        armytest, citytest = diplomacy_read(s)

        armies = {}
        armies['A'] = army_class('A', 'Madrid', 'Hold')
        armies['B'] = army_class('B', 'Barcelona', 'Move', 'Madrid')
        armies['C'] = army_class('C', 'London', 'Support', 'B')

        cities = {}
        cities['Madrid'] = city_class('Madrid', 'A')
        cities['Barcelona'] = city_class('Barcelona', 'B', 'Madrid')
        cities['London'] = city_class('London', 'C')

        for army in armies:
            self.assertEqual(armies[army], armytest[army])

        for city in cities:
            assert cities[city] == citytest[city]

    def test_read_3(self):
        s = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London')
        armytest, citytest = diplomacy_read(s)

        armies = {}
        armies['A'] = army_class('A', 'Madrid', 'Hold')
        armies['B'] = army_class('B', 'Barcelona', 'Move', 'Madrid')
        armies['C'] = army_class('C', 'London', 'Support', 'B')
        armies['D'] = army_class('D', 'Austin', 'Move', 'London')

        cities = {}
        cities['Madrid'] = city_class('Madrid', 'A')
        cities['Barcelona'] = city_class('Barcelona', 'B', 'Madrid')
        cities['London'] = city_class('London', 'C')
        cities['Austin'] = city_class('Austin', 'D', 'London')

        for army in armies:
            self.assertEqual(armies[army], armytest[army])

        for city in cities:
            assert cities[city] == citytest[city]

    # PRINT
    def test_print_1(self):
        w = StringIO()

        s = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n')
        armytest, citytest = diplomacy_read(s)

        diplomacy_print(w, armytest)
        self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\nC London\nD Austin\n")

    def test_print_2(self):
        w = StringIO()

        s = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        armytest, citytest = diplomacy_read(s)

        diplomacy_print(w, armytest)
        self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\nC London\n")

    def test_print_3(self):
        w = StringIO()

        s = StringIO("A Madrid Hold")
        armytest, citytest = diplomacy_read(s)

        diplomacy_print(w, armytest)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    # EVAL
    def test_eval_1(self):
        s = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London')
        armytest_, citytest_ = diplomacy_read(s)
        armytest, citytest = diplomacy_eval(armytest_, citytest_)

        armies = {}
        armies['A'] = army_class('A', '[dead]', None, None)
        armies['B'] = army_class('B', '[dead]', None, None)
        armies['C'] = army_class('C', '[dead]', None, None)
        armies['D'] = army_class('D', '[dead]', None, None)

        cities = {}
        cities['Madrid'] = city_class('Madrid', None, None)
        cities['Barcelona'] = city_class('Barcelona', None, None)
        cities['London'] = city_class('London', None, None)
        cities['Austin'] = city_class('Austin', None, None)

        for army in armies:
            self.assertEqual(armies[army], armytest[army])

        for city in cities:
            assert cities[city] == citytest[city]

    # SOLVE
    def test_solve_1(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_solve_2(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_solve_6(self):
        for i, input in enumerate(test_inputs):
            r = StringIO(input)
            w = StringIO()
            diplomacy_solve(r, w)
            self.assertEqual(
                w.getvalue(), test_outputs[i])



if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1
$ cat TestDiplomacy.out
...............
----------------------------------------------------------------------
Ran 15 tests in 0.003s

OK


$ coverage report -m                   >> TestDiplomacy.out
$ cat TestDiplomacy.out
...............
----------------------------------------------------------------------
Ran 15 tests in 0.003s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py         116      1     50      1    99%   39, 170->119
TestDiplomacy.py     123      0     20      0   100%
--------------------------------------------------------------
TOTAL                239      1     70      1    99%
"""
