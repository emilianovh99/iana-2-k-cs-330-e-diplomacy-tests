# TestDiplomacy.py

from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_solve
# from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

class TestDiplomacy (TestCase):
 
    def test_1(self):
        qn = 'A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London'
        w = diplomacy_solve(qn)
        self.assertEqual(w, 'A [dead]\nB [dead]\nC [dead]\nD [dead]\n')

    def test_2(self):
        qn = 'A Madrid Hold\nB Barcelona Move Madrid'
        w = diplomacy_solve(qn)
        self.assertEqual(w, 'A [dead]\nB [dead]\n')

    def test_3(self):
        qn = 'A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B'
        w = diplomacy_solve(qn)
        self.assertEqual(w, 'A [dead]\nB Madrid\nC [dead]\nD Paris\n')


if __name__ == "__main__":
    main()
